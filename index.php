<?php

require_once 'vendor/autoload.php' ;
session_start();
$app = new \Slim\Slim ;
use Illuminate\Database\Capsule\Manager as DB;

$config = parse_ini_file('src/conf/conf.ini');
$db = new DB();
$db->addConnection( $config );
$db->setAsGlobal();
$db->bootEloquent();

$app->get('/', function(){
  $app =\Slim\Slim::getInstance() ;
  $app->redirect($app->urlFor('connexion'));
})->name('racine');

$app->get('/Accueil', function(){
  $control= new \mywishlist\controller\ControllerAffichage();
  $control->getAccueil();
})->name('accueil');

$app->get('/Affichage/listesPubliques', function(){
  $control= new \mywishlist\controller\ControllerAffichage();
  $control->getListes();
})->name('afficheListes');

$app->get('/Affichage/liste/:no', function($no){
  $control= new \mywishlist\controller\ControllerAffichage();
  $control->getItemsListe($no);
})->name('afficheItemsListe');

$app->get('/Affichage/item/:id', function($id){
  $control= new \mywishlist\controller\ControllerAffichage();
  $control->getItem($id);
})->name('afficheItem');


$app->post('/Affichage/liste', function(){
  $control= new \mywishlist\controller\ControllerAffichage();
  $control->getListe($_POST['no']);
});

$app->get('/Creation/creerListe', function(){
  $control = new \mywishlist\controller\ControllerCreation();
  $control->creerListeForm();
})->name('creerListe');

$app->post('/Creation/creerListe', function(){
  $control= new \mywishlist\controller\ControllerCreation();
  $control->creerListe($_POST['titre'],$_POST['description'],$_POST['expire']);
});

$app->get('/Creation/ajoutItem', function(){
  $control = new \mywishlist\controller\ControllerCreation();
  $control->creerItemForm();
})->name('creerItem');

$app->post('/Creation/ajoutItem', function(){
  $control= new \mywishlist\controller\ControllerCreation();
  $control->creerItem($_POST['titre'],$_POST['nom'],$_POST['descr'],$_POST['img'],$_POST['tarif']);
});

$app->get('/Modification/supItem', function(){
  $control = new \mywishlist\controller\ControllerModification();
  $control->supItemForm();
})->name('supItem');

$app->post('/Modification/supItem', function(){
  $control= new \mywishlist\controller\ControllerModification();
  $control->supItem($_POST['id']);
});

$app->get('/Connexion/creeCompte',function(){
  $control= new \mywishlist\controller\ControllerConnexion();
  $control->getCreationCompte();
})->name('inscription');

$app->post('/Connexion/creeCompte',function(){
  $control= new \mywishlist\controller\ControllerConnexion();
  $control->creationCompte($_POST['nom'],$_POST['prenom'],password_hash($_POST['mdp'],PASSWORD_DEFAULT),$_POST['login']);
});

$app->get('/Connexion/seConnecter',function(){
  $control= new \mywishlist\controller\ControllerConnexion();
  $control->getConnexion();
})->name('connexion');

$app->post('/Connexion/seConnecter',function(){
  $control= new \mywishlist\controller\ControllerConnexion();
  $control->connexion($_POST['login'],$_POST['mdp']);
});

$app->get('/Modification/modifItem/:id', function($id){
  $control = new \mywishlist\controller\ControllerModification();
  $control->modifItemForm($id);
})->name('modifItem');

$app->post('/Modification/modifItem/:id', function($id){
  $control= new \mywishlist\controller\ControllerModification();
  $control->modifItem($_POST['id'],$_POST['nom'],$_POST['descr'],$_POST['tarif']);
});

$app->get('/Modification/modifListe', function(){
  $control = new \mywishlist\controller\ControllerModification();
  $control->modifListeForm();
})->name('modifListe');

$app->post('/Modification/modifListe', function(){
  $control= new \mywishlist\controller\ControllerModification();
  $control->modifListe($_POST['titre'],$_POST['t'],$_POST['description'],$_POST['expire']);
});

$app->get('/Modification/messageListe', function(){
  $control = new \mywishlist\controller\ControllerModification();
  $control->messageListeForm();
})->name('modifMsgListe');

$app->post('/Modification/messageListe', function(){
  $control = new \mywishlist\controller\ControllerModification();
  $control->messageListe($_POST['titre'],$_POST['description']);
});

$app->get('/Modification/listePublique', function(){
  $control = new \mywishlist\controller\ControllerParticipant();
  $control->listePubliqueForm();
});

$app->post('/Modification/listePublique', function(){
  $control = new \mywishlist\controller\ControllerParticipant();
  $control->listePublique($_POST['no']);
  $control->getListes();
});

$app->get('/Modification/reserver/:id', function($id){
  $control = new \mywishlist\controller\ControllerModification();
  $control->reserver($_SESSION['login'],$id);
})->name('resItem');
/*
$app->post('/Modification/reserver/:id', function($id){
	$control= new \mywishlist\controller\ControllerModification();
	$control->reserver($_SESSION['login'],$id);
})->name('resItem');
*/
$app->get('/Modification/messageRes',function(){
	$control= new \mywishlist\controller\ControllerModification();
	$control->ajoutMessResForm();
})->name('msgResItem');

$app->post('/Modification/messageRes',function(){
	$control= new \mywishlist\controller\ControllerModification();
	$itemId;
	if(isset($_SESSION["itemRes"])){
		$itemId=$_SESSION["itemRes"];
	}
	else{
		$itemId=$_POST['id'];
	}
	$control->ajoutMessRes($_SESSION["login"],$itemId,$_POST['msg']);
});

$app->get('/Modification/modifCompte',function(){
  $control= new \mywishlist\controller\ControllerModification();
  if (isset($_SESSION["login"])) {
    $control->getModifCompte($_SESSION["login"]);
  }

})->name('modifCompte');

$app->post('/Modification/modifCompte',function(){
    $control= new \mywishlist\controller\ControllerModification();
	$control->modifCompte($_POST['nom'],$_POST['prenom'],password_hash($_POST['mdp'],PASSWORD_DEFAULT));
});

$app->get('/Modification/modifImage/:id',function($id){
    $control= new \mywishlist\controller\ControllerModification();
	$control->getModifImage($id);
})->name('modifImgItem');

$app->post('/Modification/modifImage/:id',function($id){
  $control= new \mywishlist\controller\ControllerModification();
  $control->modifImage($id,$_POST['img']);
});

$app->post('/Modification/uploadImg',function(){
  $control= new \mywishlist\controller\ControllerModification();
  $destination = 'img/'.$_FILES['nom']['name'];
  $control->upload($_FILES['nom'],$destination,FALSE,array('png','gif','jpg','jpeg'),$_POST['id']);
})->name('upload');

$app->get('/Modification/partageListe',function(){
	$control= new \mywishlist\controller\ControllerModification();
	$control->partageListe();
})->name('partageListe');

$app->get('/Affichage/partageListe/:token',function($token){//Gatien
	$control=new \mywishlist\controller\ControllerAffichage();
	$control->partageListeToken($token);
});

$app->get('/Connexion/deconnexion',function(){
	$control=new \mywishlist\controller\ControllerConnexion();
	$control->deconnexion();
})->name('deconnexion');

$app->get('/Modification/supCompte',function(){
	$control=new \mywishlist\controller\ControllerModification();
	$control->supprimerCompte($_SESSION['login']);
})->name('supprimerCompte');


$app->run();
