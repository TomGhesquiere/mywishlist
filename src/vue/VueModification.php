<?php

namespace mywishlist\vue;
use mywishlist\models\Liste as Liste;
use mywishlist\models\Item as Item;
use mywishlist\models\Utilisateur as Utilisateur;

class VueModification{

  private $tab, $header, $active1, $active2, $a1, $a2, $a3, $a4, $a5, $a6;

  public function __construct($t=[]){
    $this->tab = $t;
  }

  public function afficheModifImage(){
    $id = $this->tab['id'];
    $this->header = "<link rel='stylesheet' type='text/css' href= '../../css.css' />";
    $this->active1 = "class='active'";
    $this->a1 = "class='active'";
    $app =\Slim\Slim::getInstance();
    $url = $app->urlFor('upload');
    return <<<END
	  <div class="intro">
			<div align="center">
				<table>
					<tr>
						<td align="right">
              <form name="formModif" action = "" method = "POST" >
                <input type = "hidden" name = "id" value='$id'><br><br>
                Nouvelle image : <input type = "text" name = "img" required>
								<input type = "submit" value = "Modifier" class = "bouton">
							</form>
              <form name="formSupp" action = "" method = "POST" >
                <input type = "hidden" name = "id" value='$id'>
                <input type = "hidden" name = "img" value =''>
								<input type = "submit" value = "Supprimer" class = "bouton">
							</form>
              <form name="formUpload" method="post" action='$url' enctype="multipart/form-data">
                <input type = "file" name= "nom" />
                <input type = "hidden" name = "id" value='$id'>
                <input type = "submit" value = "Uploader" class = "bouton">
              </form>
						</td>
					</tr>
				</table>
			</div>
	  </div>
END;
  }

  private function reservForm(){//Rajouter un if pour personne deja co GATIEN
    $this->header = "<link rel='stylesheet' type='text/css' href= '../css.css' />";
    $this->active1 = "class='active'";
    $this->a2 = "class='active'";
	if(isset($_SESSION["login"])){
    return <<<END
    <div class="intro">
      <div align="center">
        <table>
          <tr>
            <td align="right">
              <form id="form5" method="POST" action="">
			          ID item : <input type="text" name="id" autofocus required><br><br><br>
                <input type = "submit" value = "Valider" class = "bouton">
              </form>
            </td>
  				</tr>
  			</table>
  		</div>
    </div>
END;
	}
	else{
		$app =\Slim\Slim::getInstance();
		$urlC = $app->urlFor('connexion');
		return
		<<<END
    <div class="intro">
      <div align="center">
        <table>
          <tr>
            <td align='center'>
              Veuillez vous connecter afin de reserver un item<br>
			  <a href=$urlC><button>Aller vers le Portail de connexion</button></a>
            </td>

  				</tr>

  			</table>
  		</div>
    </div>
END;

	}

  }

  private function reserver(){
	  $utilisateur = Utilisateur::where('login','=',$this->tab[0])->first();//Mettre un if pour verifier la reservation
	  $item = Item::where('id','=',$this->tab[1])->first();
	  if(!isset($item->id_utilisateur)){
		$item->id_utilisateur= $utilisateur->user_id;
		$item->save();
		$msgValidation ="L'item a bien été reservé";
		$_SESSION["itemRes"]=$item->id;
		$app =\Slim\Slim::getInstance() ;//Gatien a finir redirection
		$app->redirect($app->urlFor('msgResItem'));
	  }
	  else{
		  $msgValidation = "L'item est déjà reservé !";
	  }
	  $this->header = "<link rel='stylesheet' type='text/css' href= '../../css.css' />";
    return <<<END
    <div class="intro">
      <div align="center">
        <table>
          <tr>
            <td align="right">
             <p>$msgValidation<p>
            </td>
          </tr>
        </table>
      </div>
    </div>
END;

  }


  public function ajoutMessResForm(){///gatien
    $this->header = "<link rel='stylesheet' type='text/css' href= '../css.css' />";
	if(isset($_SESSION["login"])){
		if(isset($_SESSION["itemRes"])){
			return <<<END
			<script type='text/javascript'> alert('La réservation a été prise en compte, ajoutez un message pour la réservation');</script>
		<div class="intro">
		  <div align="center">
			<table>
			  <tr>
				<td align="right">
				  <form method="POST" action="">
					Message : <input type="text submit" name="msg" maxlength="100" size="101" required><br><br><br>
					  <input type = "submit" value = "Valider" class = "bouton">
					</form>
				</td>
			  </tr>
			</table>
		  </div>
		</div>
END;

    }/*else{
		return <<<END
		<script type='text/javascript'> alert('Ajoutez un message sur la réservation d'un item');</script>
		<div class="intro">
		  <div align="center">
			<table>
			  <tr>
				<td align="right">
				  <form method="POST" action="">
					id Item : <input type="text submit" name="id" maxlength="7" size="6" autofocus required>
					Message : <input type="text submit" name="msg" maxlength="100" size="101" required><br><br><br>
					  <input type = "submit" value = "Valider" class = "bouton">
					</form>
				</td>
			  </tr>
			</table>
		  </div>
		</div>
END;
		}
*/
	}
	else{
	$app =\Slim\Slim::getInstance();
	$urlC = $app->urlFor('connexion');
	return <<<END
    <div class="intro">
      <div align="center">
        <table>
          <tr>
            <td align='center'>
              Veuillez vous connecter afin de réserver un item<br>
			  <a href=$urlC><button>Aller vers le portail de connexion</button></a>
            </td>
          </tr>
        </table>
      </div>
    </div>
END;
	}

  }

  public function ajoutMessRes(){
	 $item = Item::where('id','=',$this->tab["1"])->first();
	 $utilisateur = Utilisateur::where('login','=',$this->tab[0])->first();
	 if(isset($item->id_utilisateur)) {
		 if($item->id_utilisateur == $utilisateur->user_id){
			$item->msgReservation=$this->tab["2"];
			$item->save();
			$msg1="Le message de réservation a été envoyé";
      $msg2='';
      $content='';
		 }
		 else {
			 $msg2="L'item a déjà été reservé par un autre utilisateur";
		 }
	 }
	 else{
		  $content="L'item n'a pas encore été reservé, veuillez réserver l'item avant d'y ajouter un message";
	 }
	if(isset($_SESSION["itemRes"])){
		unset($_SESSION["itemRes"]);
	}
     $this->header = "<link rel='stylesheet' type='text/css' href= '../css.css' />";
	 return <<<END
    <div class="intro">
      <div align="center">
        <table>
          <tr>
            <td align="right">
              <div class="valide">
                <p>$msg1</p>
              </div>
              <p>$msg2</p>
              $content
            </td>
          </tr>
        </table>
      </div>
    </div>
END;
  }

  private function messageListeForm(){
    $this->header = "<link rel='stylesheet' type='text/css' href= '../css.css' />";
    $this->active2 = "class='active'";
    $this->a3 = "class='active'";
    $msg1 = '';
    $msg2 = '';
    if (isset($this->tab['msg1'])){
      $msg1 = $this->tab['msg1'];
    }else if (isset($this->tab['msg2'])){
      $msg2 = $this->tab['msg2'];
    }

    return <<<END
    <div class="intro">
      <div align="center">
        <table>
          <tr>
            <td align="right">
              <div class="valide">
                <p>$msg1</p>
              </div>
              <p>$msg2</p><br>
              <form id="form5" method="POST" action="">
                Titre de la liste : <input type="text" name="titre" autofocus required><br><br>
                Message : <input type="text" name="description" required><br><br><br>
                <input type = "submit" value = "Valider" class = "bouton">
              </form>
            </td>
          </tr>
        </table>
      </div>
    </div>
END;
  }

  private function modifListeForm(){
    $this->header = "<link rel='stylesheet' type='text/css' href= '../css.css' />";
    $this->active2 = "class='active'";
    $this->a4 = "class='active'";
    $msg1 = '';
    $msg2 = '';
    if (isset($this->tab['msg1'])){
      $msg1 = $this->tab['msg1'];
    }else if (isset($this->tab['msg2'])){
      $msg2 = $this->tab['msg2'];
    }

    return <<<END
    <div class="intro">
      <div align="center">
        <table>
          <tr>
            <td align="right">
              <div class="valide">
                <p>$msg1</p>
              </div>
              <p>$msg2</p><br>
              <form id="form5" method="POST" action="">
                Titre de la liste à modifier : <input type="text" name="titre" autofocus required><br><br>
                Nouveau titre : <input type="text" name="t" required><br><br>
                Nouvelle description : <input type="text" name="description" required><br><br>
                Nouvelle date d'expiration : <input type="date" name="expire" required><br><br><br>
                <input type = "submit" value = "Valider" class = "bouton">
              </form>
            </td>
          </tr>
        </table>
      </div>
    </div>
END;
  }

  private function modifItemForm(){
    $id = $this->tab['id'];
    $this->header = "<link rel='stylesheet' type='text/css' href= '../../css.css' />";
    $this->active1 = "class='active'";
    $this->a5 = "class='active'";
    $msg = '';
    if (isset($this->tab['msg'])){
      $msg = $this->tab['msg'];
    }

    return <<<END
    <div class="intro">
      <div align="center">
        <table>
          <tr>
            <td align="right">
              <p>$msg</p>
              <form id="form5" method="POST" action="">
               <input type="hidden" name="id" value='$id'><br><br>
                Nouveau nom de l'item : <input type="text" name="nom" ><br><br>
                Description : <input type="text" name="descr" ><br><br>
                Tarif : <input type="text" name="tarif" ><br><br><br>
                <input type = "submit" value = "Valider" class = "bouton">
              </form>
            </td>
          </tr>
        </table>
      </div>
    </div>
END;
  }

  private function supItemForm(){
    $this->header = "<link rel='stylesheet' type='text/css' href= '../css.css' />";
    $this->active1 = "class='active'";
    $this->a6 = "class='active'";
    $msg1 = '';
    $msg2 = '';
    if (isset($this->tab['msg1'])){
      $msg1 = $this->tab['msg1'];
    }else if (isset($this->tab['msg2'])){
      $msg2 = $this->tab['msg2'];
    }
    return <<<END
    <div class="intro">
      <div align="center">
        <table>
          <tr>
            <td align="right">
              <div class="valide">
                <p>$msg1</p>
              </div>
              <p>$msg2</p><br>
              <form id="form4" method="POST" action="">
                ID item : <input type="text" name="id" autofocus required><br><br><br>
                <input type = "submit" value = "Valider" class = "bouton">
              </form>
            </td>
          </tr>
        </table>
      </div>
    </div>
END;
  }

  public function afficheUrlPartage(){//Gatien
  $this->header = "<link rel='stylesheet' type='text/css' href= '../css.css' />";
    $this->active2 = "class='active'";
	$urlPartage=$this->tab[1];


	  return <<<END
    <div class="intro">
      <div align="center">
        <table>
          url pour partager votre liste :<br>localhost/mywishlist/Affichage/partageListe/$urlPartage
  			</table>
  		</div>
    </div>
END;
  }

  private function listePubliqueForm(){
    $content = '<form id="form5" method="POST" action="">
                  <input type="text" name="no" placeholder="no" required>
                  <button type="submit">Valider</button>
                </form>';
    return $content;
  }

  private function listePublique(){
    $liste = Liste::find($this->tab['no']);
    $liste->publique = true;
    $liste->save();
  }

  private function getModifCompte(){
    $utilisateur = Utilisateur::where("login","=",$_SESSION['login'])->first();
    $nom = $utilisateur->nom;
    $prenom = $utilisateur->prenom;
    $app =\Slim\Slim::getInstance();
    $urlSupp = $app->urlFor('supprimerCompte');
    $this->header = "<link rel='stylesheet' type='text/css' href= '../css.css' />";
    return <<<END
    <script type="text/javascript">
		<!--

		function verif_formulaire()
		{
      if(document.formulaire.mdp.value != null)  {
 		   if (document.formulaire.cmdp.value == null) {
         alert("Veuillez confirmer le mot de passe");
        document.formulaire.cmdp.focus();
        return false;
      }else {
        if(document.formulaire.mdp.value != document.formulaire.cmdp.value)  {
   		   alert("Veuillez entrer le même mot de passe");
   		   document.formulaire.cmdp.focus();
   		   return false;
   		  }
      }
 		  }

		}
		//-->
		</script>
    <div class="intro">
  			<div align="center">
  				<table>
  					<tr>
  						<td align="right">
                <form name="formulaire" action = "" method = "POST" onSubmit="return verif_formulaire()">
                  Nouveau Nom : <input type = "text" name = "nom" placeholder='$nom'> <br><br>
                  Nouveau Prénom : <input type = "text" name = "prenom" placeholder='$prenom'> <br><br>
  								Nouveau Mot de passe : <input type = "password" name = "mdp" > <br><br>
                  Confirmer le nouveau mot de passe : <input type = "password" name = "cmdp" > <br><br><br>
  								<input type = "submit" value = "Valider" class = "bouton">
  							</ form>
                <br>
                <a href='$urlSupp' class='bouton'>Supprimer Mon Compte</a>
  						</td>
  					</tr>
  				</table>
  			</div>
  	</div>
END;
  }

  public function render($sel){
    $app =\Slim\Slim::getInstance();
		$urlA = $app->urlFor('accueil');
		$urlL = $app->urlFor('afficheListes');
		$urlCL = $app->urlFor('creerListe');
		$urlML = $app->urlFor('modifListe');
		$urlMML = $app->urlFor('modifMsgListe');
		$urlCI = $app->urlFor('creerItem');
		$urlMI = $app->urlFor('modifItem');
		$urlRI = $app->urlFor('resItem');
		$urlMII = $app->urlFor('modifImgItem');
		$urlSI = $app->urlFor('supItem');
    $urlMC = $app->urlFor('modifCompte');
    $urlDECO = $app->urlFor('deconnexion');


    switch ($sel) {
      case 1:
          $content = $this->afficheModifImage();
          break;
      case 2 :
  		    $content=$this->reservForm();
  		    break;
      case 3 :
          $content=$this->reserver($this->tab);
  		    break;
      case 4 :
    		  $content=$this->ajoutMessResForm();
    		  break;
    	case 5 :
    		  $content=$this->ajoutMessRes();
    		  break;
      case 6 :
          $content=$this->messageListeForm();
          break;
      case 7 :
          $content=$this->modifListeForm();
          break;
      case 8 :
          $content=$this->modifItemForm();
          break;
      case 9 :
          $content=$this->supItemForm();
          break;
		  case 10:
          $content = $this->afficheUrlPartage();
          break;
      case 11 :
          $content=$this->listePubliqueForm();
          break;
    	case 12:
          $content = $this->listePublique();
          break;
      case 13:
          $content = $this->getModifCompte();
          break;

      default:
        // code...
        break;
    }
    $html = <<<END
    <!DOCTYPE html>
    <html>
    <head>
      <title>MyWishlist</title>
      $this->header
      <meta charset="utf-8"/>
    </head>
    <body>
    <a style = 'text-decoration:none' href='$urlA'><h1>MyWishlist</h1></a>
    <div class="menu">
     <ul id="nav">
        <li><a href='$urlL' $this->active2>Mes listes</a>
          <ul>
            <li><a href='$urlCL'>Créer une liste</a></li>
            <li><a href='$urlML' $this->a4>Modifier une liste</a></li>
            <li><a href='$urlMML' $this->a3>Modifier le message d'une liste</a></li>
          </ul>
        </li>
        <li><a href='' $this->active1>Mes items</a>
          <ul>
          <li><a href='$urlCI'>Créer un item</a></li>
          <!--
          <li><a href='$urlMI'>Modifier un item</a></li>
          <li><a href='$urlMII'>Modifier l'image d'un item</a></li>
          <li><a href='$urlRI'>Réserver un item</a></li>
          -->
          <li><a href='$urlSI' $this->a6>Supprimer un item</a></li>
            </ul>
        </li>
        <li><a href='$urlMC'>Mon compte</a></li>
        <li style="float:right"><a href='$urlDECO'>Se déconnecter</a></li>
     </ul>
    </div>
    <div class="content">
      $content
    </div>
    </body></html>
END;

    echo $html;
  }
}
