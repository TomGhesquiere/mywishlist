<?php

namespace mywishlist\vue;
use mywishlist\models\Liste as Liste;
use mywishlist\models\Item as Item;


class VueConnexion{

  private $tab;

  public function __construct($t=[]){
    $this->tab = $t;
  }

  private function afficheConnexion(){
    $app =\Slim\Slim::getInstance();
    $urlI = $app->urlFor('inscription');
    $msg = '';
    if (isset($this->tab['msg'])){
      $msg = $this->tab['msg'];
    }

    return <<<END
	  <div class="intro">
		<h4>Êtes-vous déjà membre ?</h4>

		<div align="center">
			<table>
				<tr>
					<td align="right">
						<div class="id">
              <p>$msg</p><br>
							<form action = "" method = "POST">
								Login : <input type = "text" name = "login" autofocus required> <br><br>
								Mot de passe : <input type = "password" name = "mdp" required> <br><br><br>
								<input type = "submit" value = "Se Connecter" class = "bouton">
							</ form>
						</div>
					</td>
				</tr>
			</table>
		</div>

	</div>

	<div class="tout">
		<div class="texte">
			<h4>Rejoignez-nous :</h4>
		</div>

		<div class="compte">
			<a href='$urlI'><input type="button" class="bouton" style="width:120px" value="Créer un compte"/>
		</div>
	</div>
END;
  }

  private function afficheCreationCompte(){
    $msg = '';
    if (isset($this->tab['msg'])){
      $msg = $this->tab['msg'];
    }

    return <<<END
    <script type="text/javascript">
		<!--

		function verif_formulaire()
		{
		 if(document.formulaire.mdp.value != document.formulaire.cmdp.value)  {
		   alert("Veuillez entrer le même mot de passe");
		   document.formulaire.cmdp.focus();
		   return false;
		  }
		}
		//-->
		</script>
	<div class="intro">
			<div align="center">
        <p>$msg</p><br>
				<table>
					<tr>
						<td align="right">
              <form name="formulaire" action = "" method = "POST" onSubmit="return verif_formulaire()">
                Login : <input type = "text" name = "login" autofocus required> <br><br>
                Nom : <input type = "text" name = "nom" required> <br><br>
                Prénom : <input type = "text" name = "prenom" required> <br><br>
								Mot de passe : <input type = "password" name = "mdp" required> <br><br>
                Confirmer le mot de passe : <input type = "password" name = "cmdp" required> <br><br><br>
								<input type = "submit" value = "Valider" class = "bouton">
							</ form>
						</td>
					</tr>
				</table>
			</div>
	</div>
END;
}

  public function render($sel){
    $app =\Slim\Slim::getInstance();
    $urlC = $app->urlFor('connexion');

    switch ($sel) {
      case 1:
        $content = $this->afficheConnexion();
        break;
      case 2:
          $content = $this->afficheCreationCompte();
        break;

      default:

        break;
    }
    $html = <<<END
    <!DOCTYPE html>
    <html>
    <head>
      <title>MyWishlist</title>
      <link rel="stylesheet" type="text/css" href= "../css.css" />
      <meta charset="utf-8"/>
    </head>
    <body>
    <a style = "text-decoration:none" href='$urlC'> <h1>MyWishlist</h1></a>
    <div class="content">
      $content
    </div>
    </body></html>
END;

    echo $html;
  }
}
