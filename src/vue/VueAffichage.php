<?php

namespace mywishlist\vue;
use mywishlist\models\Liste as Liste;
use mywishlist\models\Item as Item;
use mywishlist\models\Utilisateur as Utilisateur;

class VueAffichage{

	private $tab, $header, $active;

	 public function __construct($t=[]){
		$this->tab=$t;
		$this->header = "";
		$this->active = "";
	}

	private function affListesSouhaits(){
		$this->header = "<link rel='stylesheet' type='text/css' href= '../css.css' />";
		$this->active = "class='active'";
		$content = "<div class='intro'><br>";
		foreach ($this->tab as $liste) {
			$app =\Slim\Slim::getInstance() ;
			$url = $app->urlFor('afficheItemsListe', ['no'=> $liste['no']] ) ;
			$content = $content."<a href='$url'>$liste[titre]</a><br><br>";
		}
		$content = $content."</div>";
    return $content;
  }

    private function affListeItems(){//Gatien faire avec les login et mdp de co
			$app =\Slim\Slim::getInstance();
			$this->header = "<link rel='stylesheet' type='text/css' href= '../../css.css' />";
			$this->active = "class='active'";
			$content="";

		/**
		*On test si le créateur de la liste est identifié par un cookie ou si il est "logger"
		*/
		$liste = $this->tab[0];
		$utilisateur=$this->tab[1];
		$urlP = $app->urlFor('partageListe');

		$content=$content."<div class='intro'><br>";
    	$liste = $this->tab[0];
    	$content = $content."<h3>$liste->titre</h3><h5>$liste->description</h5><h5>Prend fin le : ".date('d/m/Y', strtotime($liste->expiration))."</h5><br>";
    	$items = $liste->items;

    	foreach ($items as $item) {
      	$url = $app->urlFor('afficheItem', ['id'=> $item['id']] ) ;
      	$content = $content."<a href='$url'>".$item['nom']."</a><br><br>";
    	}

			$content = $content."</div>";

			if(isset($_COOKIE["mwl"]) && $liste->user_id==$_COOKIE["mwl"] ){
				$content=$content."<div class='tout'><div class='compte'><a href='$urlP'><input type='button' class='bouton' style='width:120px' value='Partager'/></a></div></div>";
				$_SESSION["no"]=$liste->no;

			}
			else if(isset($_SESSION["login"]) && $liste->user_id == $utilisateur->user_id){
				$content=$content."<div class='tout'><div class='compte'><a href='$urlP'><input type='button' class='bouton' style='width:120px' value='Partager'/></a></div></div>";
				$_SESSION["no"]=$liste->no;
			}

    	return $content;
	}

  private function affItem(){
		$app =\Slim\Slim::getInstance();
		$this->header = "<link rel='stylesheet' type='text/css' href= '../../css.css' />";
		$this->active = "class='active'";
    $content = "<div class='intro'><br>";
    $item=$this->tab[0];
    $url = $app->urlFor('racine') ;
		$urlModif = $app->urlFor('modifItem', ['id'=> $item->id] );
		$urlRes = $app->urlFor('resItem', ['id'=> $item->id]);
		$urlSuppImage = $app->urlFor('modifImgItem', ['id'=> $item->id]);
    $content = $content."<a href='$urlSuppImage'><img src ='$url"."img/". $item->img."' height='200' width='200'></a><br><br>";
		$content = $content."ID : ".$item->id.'<br><br>Nom : '.$item->nom.'<br><br>Description : '.$item->descr."<br><br>Prix : ".$item->tarif."€<br><br><br>";
		$content = $content."<a href='$urlModif'><input type='button' class='bouton' style='width:120px' value='Modifier'/></a><br><br>";
		if (isset($item->id_utilisateur)) {
			$content = $content."Déjà Reservé";
		}else {
			$content = $content."<a href='$urlRes'><input type='button' class='bouton' style='width:120px' value='Réserver'/></a>";
		}
		$content = $content."</div>";
    return $content;
  }

	private function accueilSite(){
		$this->header = "<link rel='stylesheet' type='text/css' href= 'css.css' />";
  }

	public function render($sel){
		$app =\Slim\Slim::getInstance();
		$urlA = $app->urlFor('accueil');
		$urlL = $app->urlFor('afficheListes');
		$urlCL = $app->urlFor('creerListe');
		$urlML = $app->urlFor('modifListe');
		$urlMML = $app->urlFor('modifMsgListe');
		$urlCI = $app->urlFor('creerItem');
		$urlMI = $app->urlFor('modifItem');
		$urlRI = $app->urlFor('resItem');
		$urlMII = $app->urlFor('modifImgItem');
		$urlSI = $app->urlFor('supItem');
		$urlMC = $app->urlFor('modifCompte');
		$urlDECO = $app->urlFor('deconnexion');

		switch($sel){
			case 1:
				$content = $this->affListesSouhaits();
				break;
			case 2:
				$content = $this->affListeItems();
				break;
			case 3:
				$content=$this->affItem();
				break;
			case 4:
				$content=$this->accueilSite();
				break;

			default:

			break;
		}

		 $html = <<<END
			<!DOCTYPE html>
			<html>
			<head>
	      <title>MyWishlist</title>
				$this->header
	      <meta charset="utf-8"/>
	    </head>
			<body>
			<a style = 'text-decoration:none' href='$urlA'><h1>MyWishlist</h1></a>
			<div class="menu">
	     <ul id="nav">
	        <li><a href='$urlL' $this->active>Mes listes</a>
						<ul>
	      			<li><a href='$urlCL'>Créer une liste</a></li>
	      			<li><a href='$urlML'>Modifier une liste</a></li>
	      			<li><a href='$urlMML'>Modifier le message d'une liste</a></li>
	    			</ul>
					</li>
	        <li><a href=''>Mes items</a>
						<ul>
							<li><a href='$urlCI'>Créer un item</a></li>
							<!--
							<li><a href='$urlMI'>Modifier un item</a></li>
							<li><a href='$urlMII'>Modifier l'image d'un item</a></li>
							<li><a href='$urlRI'>Réserver un item</a></li>
							-->
							<li><a href='$urlSI'>Supprimer un item</a></li>
							</ul>
					</li>
	        <li><a href='$urlMC'>Mon compte</a></li>
	        <li style="float:right"><a href='$urlDECO'>Se déconnecter</a></li>
	     </ul>
	   	</div>
			<div class="content">
				$content
			</div>
			</body></html>
END;

    echo $html;
	}
 }
