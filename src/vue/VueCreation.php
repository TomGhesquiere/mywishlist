<?php

namespace mywishlist\vue;
use mywishlist\models\Liste as Liste;
use mywishlist\models\Item as Item;
use mywishlist\models\Utilisateur as Utilisateur;


class VueCreation{

  private $tab, $active1, $active2, $a1, $a2;

  public function __construct($t=[]){
    $this->tab=$t;
  }

  private function affListe(){ //c est quoi cette methode ?
    $content = $this->tab[0]->titre."<br>";
    return $content;
  }

  private function formulaire(){ //c est quoi cette methode ?
    $content = '<form id="form1" method="POST" action="liste">
                  <input type="text" name="no" required>
                  <button type="submit">Valider</button>
                </form>';
    return $content;
  }

  private function creerListeForm(){
    $this->active1 = "class='active'";
    $this->a1 = "class='active'";
    $msg = '';
    if (isset($this->tab['msg'])){
      $msg = $this->tab['msg'];
    }

    return <<<END
	  <div class="intro">
			<div align="center">
				<table>
					<tr>
						<td align="right">
              <p>$msg</p><br>
              <form id="form2" method="POST" action="">
                Titre : <input type="text" name="titre" autofocus required><br><br>
                Description : <input type="text" name="description" required><br><br>
                Date d'expiration : <input type="date" name="expire" required><br><br><br>
                <input type = "submit" value = "Valider" class = "bouton">
              </form>
            </td>
    			</tr>
    		</table>
    	</div>
    </div>
END;
  }

  private function creerItemForm(){
    $this->active2 = "class='active'";
    $this->a2 = "class='active'";
    $msg = '';
    if (isset($this->tab['msg'])){
      $msg = $this->tab['msg'];
    }

    return <<<END
	  <div class="intro">
			<div align="center">
				<table>
					<tr>
						<td align="right">
              <p>$msg</p><br>
              <form id="form3" method="POST" action="">
                Titre de la liste : <input type="text" name="titre" autofocus required><br><br>
                Nom de l'item : <input type="text" name="nom" required><br><br>
                Description : <input type="text" name="descr" required><br><br>
                Nom de l'image : <input type="text" name="img"><br><br>
                Tarif : <input type="text" name="tarif" required><br><br><br>
                <input type = "submit" value = "Valider" class = "bouton">
              </form>
            </td>
        	</tr>
        </table>
      </div>
    </div>
END;
  }

  public function render($sel){
    $app =\Slim\Slim::getInstance();
		$urlA = $app->urlFor('accueil');
		$urlL = $app->urlFor('afficheListes');
		$urlCL = $app->urlFor('creerListe');
		$urlML = $app->urlFor('modifListe');
		$urlMML = $app->urlFor('modifMsgListe');
		$urlCI = $app->urlFor('creerItem');
		$urlMI = $app->urlFor('modifItem');
		$urlRI = $app->urlFor('resItem');
		$urlMII = $app->urlFor('modifImgItem');
		$urlSI = $app->urlFor('supItem');
    $urlMC = $app->urlFor('modifCompte');
    $urlDECO = $app->urlFor('deconnexion');

    switch ($sel) {
      case 1:
        $content=$this->affListe();
        break;
      case 2:
        $content=$this->formulaire();
        break;
      case 3:
          $content=$this->creerListeForm();
          break;
      case 4:
          $content=$this->creerItemForm();
          break;

      default:
        // code...
        break;
    }

    $html = <<<END
    <!DOCTYPE html>
    <html>
    <head>
      <title>MyWishlist</title>
      <link rel='stylesheet' type='text/css' href= '../css.css' />
      <meta charset="utf-8"/>
    </head>
    <body>
    <a style = 'text-decoration:none' href='$urlA'><h1>MyWishlist</h1></a>
    <div class="menu">
     <ul id="nav">
        <li><a href='$urlL' $this->active1>Mes listes</a>
          <ul>
            <li><a href='$urlCL' $this->a1>Créer une liste</a></li>
            <li><a href='$urlML'>Modifier une liste</a></li>
            <li><a href='$urlMML'>Modifier le message d'une liste</a></li>
          </ul>
        </li>
        <li><a href='' $this->active2>Mes items</a>
          <ul>
          <li><a href='$urlCI' $this->a2>Créer un item</a></li>
          <!--
          <li><a href='$urlMI'>Modifier un item</a></li>
          <li><a href='$urlMII'>Modifier l'image d'un item</a></li>
          <li><a href='$urlRI'>Réserver un item</a></li>
          -->
          <li><a href='$urlSI'>Supprimer un item</a></li>
            </ul>
        </li>
        <li><a href='$urlMC'>Mon compte</a></li>
        <li style="float:right"><a href='$urlDECO'>Se déconnecter</a></li>
     </ul>
    </div>
    <div class="content">
      $content
    </div>
    </body></html>
END;

    echo $html;
  }
}
