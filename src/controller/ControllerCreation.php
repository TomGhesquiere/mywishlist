<?php
namespace mywishlist\controller;
use mywishlist\models\Item as Item;
use mywishlist\models\Liste as Liste;
use mywishlist\models\Utilisateur as Utilisateur;

class ControllerCreation{



  public function getFormulaire(){ //c est quoi cette methode ?
      $v = new \mywishlist\vue\VueCreation([]);
      $v->render(2);
  }

  public function creerListeForm($t=[]){
      $v = new \mywishlist\vue\VueCreation($t);
      $v->render(3);
  }

  public function creerListe($titre,$description,$expire){
      $user_id = Utilisateur::where('login','=',$_SESSION['login'])->first();
      $t = Liste::where('titre','=',$titre)->first();
      if($t == null){
        $l = new Liste();
        $l->user_id = $user_id->user_id;
        $l->titre = $titre;
        $l->description = $description;
        $l->expiration = $expire;
        $l->publique = 1;
        $l->save();
        $app =\Slim\Slim::getInstance();
        $app->redirect($app->urlFor('afficheListes'));
      }
      else $this->creerListeForm($t = array('msg' => "Une liste porte déjà ce titre"));
  }

  public function creerItemForm($t=[]){
      $v = new \mywishlist\vue\VueCreation($t);
      $v->render(4);
  }

  public function creerItem($titre,$nom,$descr,$img,$tarif){
    $liste_id = Liste::where('titre','=',$titre)->first();
    $n = Item::where('nom','=',$nom)->first();
    if($liste_id == null){
      $this->creerItemForm($t = array('msg' => "Le titre n'existe pas"));
    }else if($n == null){
      $i = new Item();
      $i->liste_id = $liste_id->no;
      $i->nom = $nom;
      $i->descr = $descr;
      $i->img = $img;
      $i->tarif = $tarif;
      $i->save();
      $id = Item::where('nom','=',$nom)->first();
      $app =\Slim\Slim::getInstance();
      $app->redirect($app->urlFor('afficheItem', ['id'=> $id->id] ));
      }
      else $this->creerItemForm($t = array('msg' => "Un item porte déjà ce nom"));
    }
}
