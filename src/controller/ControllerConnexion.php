<?php
namespace mywishlist\controller;
use mywishlist\models\Utilisateur as Utilisateur;

class ControllerConnexion{

  public function getConnexion($t=[]){
    $v = new \mywishlist\vue\VueConnexion($t);
    $v->render(1);
  }

  public function connexion($login,$mdp){
    $app =\Slim\Slim::getInstance() ;
    $user = Utilisateur::where("login","=",$login)->first();
    if ($user != null) {
      if (password_verify($mdp, $user->mdp)) {
		  $_SESSION["login"]=$login;
		  //$_SESSION["mdp"]=$user->mdp;
		  setCookie("mwl",$user->user_id, time()+3600*24*365,"/mywishlist");
        $app->redirect($app->urlFor('accueil'));
      }else {
        echo password_hash($_POST['mdp'],PASSWORD_DEFAULT).'<br>';
        echo  $user->mdp;
        $this->getConnexion($t = array('msg' => "Login ou mot de passe incorrect")); //Mot de passe incorrect
      }
    }else {
      $this->getConnexion($t = array('msg' => "Login ou mot de passe incorrect ")); //Compte inexistant
    }
  }

  public function getCreationCompte($t=[]){
    $v = new \mywishlist\vue\VueConnexion($t);
    $v->render(2);
  }

  public function creationCompte($nom,$prenom,$mdp,$login){
    $app =\Slim\Slim::getInstance();
    $log = Utilisateur::where("login","=",$login)->first();
    if($log != null){
      $this->getCreationCompte($t = array('msg' => "Login déjà existant"));
    }
    else{
      $user = new Utilisateur();
      $user->nom = $nom;
      $user->prenom = $prenom;
      $user->mdp = $mdp;
      $user->login = $login;
      $user->save();
      $_SESSION['login']=$login;
		  //$_SESSION['mdp']=$user->mdp;
		  setCookie('mwl',$user->user_id, time()+3600*24*365,"/mywishlist");
      $app->redirect($app->urlFor('accueil'));
    }
  }

  public function deconnexion($t=[]){
    session_destroy();
    $app =\Slim\Slim::getInstance();
    $app->redirect($app->urlFor('connexion'));
  }


}
