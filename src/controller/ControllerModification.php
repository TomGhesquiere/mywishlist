<?php
namespace mywishlist\controller;
use mywishlist\models\Item as Item;
use mywishlist\models\Liste as Liste;
use mywishlist\models\Utilisateur as Utilisateur;

class ControllerModification{

  public function getModifImage($id) {
    $t['id']=$id;
    $v = new \mywishlist\vue\VueModification($t);
    $v->render(1);
  }

  public function modifImage($id,$img) {
    $item  = Item::find($id);
    $item->img = $img;
    $item->save();
    $app =\Slim\Slim::getInstance();
    $app->redirect($app->urlFor('afficheItem',['id'=> $id]));
  }

  function upload($fichier,$destination,$maxsize=FALSE,$extensions=array('png','gif','jpg','jpeg'),$id){
   //Test1: fichier correctement uploadé
     if (!isset($fichier) OR $fichier['error'] > 0) return FALSE;
   //Test2: taille limite
     if ($maxsize !== FALSE AND $fichier['size'] > $maxsize) return FALSE;
   //Test3: extension
     $ext = substr(strrchr($fichier['name'],'.'),1);
     if ($extensions !== FALSE AND !in_array($ext,$extensions)) return FALSE;
   //Déplacement
    move_uploaded_file($fichier['tmp_name'],$destination);
    $this->modifImage($id,$fichier['name']);
}

  public function reserverForm(){
   $v = new \mywishlist\vue\VueModification([]);
   $v->render(2);
 }

 public function reserver($log, $id){
    $v = new \mywishlist\vue\VueModification([$log,$id]);
    $v->render(3);
 }

 public function ajoutMessResForm(){
   $v = new \mywishlist\vue\VueModification([]);
   $v->render(4);
 }

 public function ajoutMessRes($log, $id , $message){
   $v = new \mywishlist\vue\VueModification([$log , $id, $message]);
   $v->render(5);
 }

 public function messageListeForm($t=[]){
   $v = new \mywishlist\vue\VueModification($t);
   $v->render(6);
 }

 public function messageListe($titre,$description){
   $liste = Liste::where('titre','=',$titre)->first();
   if($liste == null){
     $this->messageListeForm($t = array('msg2' => "La liste n'existe pas"));
   }
   else{
     $liste->description = $description;
     $liste->save();
     $this->messageListeForm($t = array('msg1' => "Le message de la liste a bien été modifiée"));
   }
 }

 public function modifListeForm($t=[]){
     $v = new \mywishlist\vue\VueModification($t);
     $v->render(7);
 }

 public function modifListe($titre,$t,$description,$expire){
    $liste = Liste::where('titre','=',$titre)->first();
    if($liste != null){
      $liste->titre = $t;
      $liste->description = $description;
      $liste->expiration = $expire;
      $liste->save();
      $this->modifListeForm($t = array('msg1' => "La liste a bien été modifiée"));
    }
    else $this->modifListeForm($t = array('msg2' => "La liste n'existe pas"));
 }

 public function modifItemForm($id, $t=[]){
    $t['id']=$id;
     $v = new \mywishlist\vue\VueModification($t);
     $v->render(8);
 }

 public function modifItem($id,$nom,$descr,$tarif){
   $app =\Slim\Slim::getInstance();
   $t = [];
   if ($nom != null) {
     $t['nom'] = $nom;
   }
   if ($descr != null) {
     $t['descr'] = $descr;
   }
   if ($tarif != null) {
     $t['tarif'] = $tarif;
   }

   $item = Item::where('id','=',$id)->first();
   $n = Item::where('nom','=',$nom)->first();
   if($n == null){
     foreach ($t as $key => $value) {
        if($value != null && $key != 'id'){
          $item->$key = $value;
          $item->save();
          $app->redirect($app->urlFor('afficheItem',['id'=> $id]));
        }
      }
   }
   else $this->modifItemForm($id, $t = array('msg' => "Un item porte déjà ce nom"));
 }

 public function supItemForm($t=[]){
     $v = new \mywishlist\vue\VueModification($t);
     $v->render(9);
 }

 public function supItem($id){
   $item = Item::where('id','=',$id)->first();
   if ($item != null){
     $item->delete();
     $this->supItemForm($t = array('msg1' => "L'item a bien été supprimé"));
   }
   else{
     $this->supItemForm($t = array('msg2' => "L'item n'existe pas"));
   }
 }

 public function partageListe(){
  if(isset($_SESSION["no"])){
    $liste = \mywishlist\models\Liste::find($_SESSION["no"]);
    //generer et modifier le new token
    $liste->token=bin2hex(random_Bytes(32));
    $liste->save();
    if(isset($_COOKIE["mwl"])){
      $v = new \mywishlist\vue\VueModification([$_COOKIE["mwl"],$liste->token]);
    }
    else{
      $utilisateur = Utilisateur::where("login","=",$_SESSION["login"])->first();
      $v = new \mywishlist\vue\VueModification([$utilisateur->user_id,$liste->token]);
    }

    $v->render(10);
  }
  else {
    echo "Veuillez choisir un item d'une de vos liste pour pouvoir le partager ";
  }
 }

 public function listePubliqueForm(){
    $v = new \mywishlist\vue\VueParticipant([]);
    $v->render(11);
  }

  public function listePublique($no){
    $v = new \mywishlist\vue\VueParticipant(['no' => $no]);
    $v->render(12);
  }

  public function getModifCompte($login){
    $utilisateur = Utilisateur::where("login","=",$login)->first();
    $t['id'] = $utilisateur->user_id;
    $v = new \mywishlist\vue\VueModification($t);
    $v->render(13);
  }
  

  public function modifCompte($login,$nom,$prenom,$mdp){

    $app =\Slim\Slim::getInstance();
    $t = [];
    $utilisateur = Utilisateur::where('login','=',$_SESSION['login'])->first();
    if ($nom != null) {
      $t['nom'] = $nom;
    }
    if ($prenom != null) {
      $t['prenom'] = $prenom;
    }
    if ($mdp != null) {
      $t['mdp'] = $mdp;
    }

    $utilisateur = Utilisateur::where('login','=',$_SESSION['login'])->first();
    foreach ($t as $key => $value) {
       if($value != null){
       $utilisateur->$key = $value;
       }
   }
    $utilisateur->save();
   $app->redirect($app->urlFor('modifCompte'));
  }


  public function supprimerCompte($login){
    $user = Utilisateur::where("login","=",$login)->first();
    $user->delete();
    session_destroy();
    $app =\Slim\Slim::getInstance();
    $app->redirect($app->urlFor('connexion'));
  }

}
