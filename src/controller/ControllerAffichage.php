<?php

namespace mywishlist\controller;
use mywishlist\models\Item as Item;
use mywishlist\models\Liste as Liste;
use mywishlist\models\Utilisateur as Utilisateur;

class ControllerAffichage{


 public function getListes(){
    $reqListes = Liste::select('no','titre','description')->where('publique','=',true);
    $listes = $reqListes->get();
    $v = new \mywishlist\vue\VueAffichage($listes->toArray());
    $v->render(1);
  }

  public function getListe($no){
      $liste = \mywishlist\models\Liste::find($no);
      //var_dump($item);
      $v = new \mywishlist\vue\VueCreation([$liste]);
      $v->render(1);
  }

    public function getItemsListe($no){
      $liste = Liste::find($no);
	    $utilisateur=null;
	    if(isset($_SESSION["login"])){
		     $utilisateur = Utilisateur::where("login","=",$_SESSION["login"])->first();
	    }
      $v = new \mywishlist\vue\VueAffichage([$liste, $utilisateur]);
      $v->render(2);
  }

   public function getItem($idItem){
      $item = Item::find($idItem);
      $v = new \mywishlist\vue\VueAffichage([$item]);
      $v->render(3);
  }

  public function partageListeToken($token){
	  $utilisateur=null;
	  if(isset($_SESSION["login"])){
		$utilisateur = Utilisateur::where("login","=",$_SESSION["login"])->first();
	  }
	  $liste=Liste::where("token","=",$token)->first();
	  $v = new \mywishlist\vue\VueAffichage([$liste,$utilisateur]);
    $v->render(2);
  }

  public function getAccueil(){
     $v = new \mywishlist\vue\VueAffichage();
     $v->render(4);
 }

}
