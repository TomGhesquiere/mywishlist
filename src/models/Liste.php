<?php

namespace mywishlist\models;
Use \Illuminate\Database\Eloquent\Model as Model;

class Liste extends Model{
  protected $table = 'liste';
  protected $primaryKey = 'no';
  public $timestamps = false;

  public function items(){
    return $this->hasMany('\mywishlist\models\Item','liste_id');
  }
}
