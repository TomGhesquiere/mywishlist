<?php

namespace mywishlist\models;
Use \Illuminate\Database\Eloquent\Model as Model;

class Item extends Model{
  protected $table = 'item';
  protected $primaryKey = 'id';
  public $timestamps = false;

  public function liste(){
    return $this->belongsTo('\mywishlist\models\Liste','liste_id');
  }

}
