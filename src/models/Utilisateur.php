<?php

namespace mywishlist\models;
Use \Illuminate\Database\Eloquent\Model as Model;

class Utilisateur extends Model{
  protected $table = 'utilisateur';
  protected $primaryKey = 'user_id';
  public $timestamps = false;
}